const api_key = '0aacc9206683ceb3a3660d1bbeb74ea6'
const img = 'https://image.tmdb.org/t/p/w500'
var strSearch;

async function evtHome(){
    const reqStr = `https://api.themoviedb.org/3/movie/top_rated?api_key=${api_key}`;

    loading();
    loadPageMovies(reqStr, 1);
}

async function evtSubmit(e){  
    e.preventDefault();
    strSearch = $("#search").val();
    if (document.getElementById('choice1').checked) {
       choose = document.getElementById('choice1').value;
      }
    else{
       choose = document.getElementById("choice2").value;
    }
    
    if(strSearch.length == 0) 
        strSearch = 'a';
    
    if(choose === "movies"){
        const reqStr = `https://api.themoviedb.org/3/search/movie?api_key=${api_key}&query=${strSearch}`;

        loading();
        loadPageMovies(reqStr, 1);
    }
    else{
        const reqStr = `https://api.themoviedb.org/3/search/person?api_key=${api_key}&query=${strSearch}`;

        loading();
        const response = await fetch(reqStr);
        const rs = await response.json();

        loadMoviesFromActor(rs.results);
    }
    
}

function loading(){
    $('#main').empty();
    $('#main').append(`
    <div class="d-flex justify-content-center">
        <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
        <strong><br/>Loading...</strong>
    </div>
    `);
}

async function loadPageMovies(reqStr, page){
    loading();
    var per_page, next_page;
    const newReqStr = `${reqStr}&page=${page}`;
    const response = await fetch(newReqStr);
    const rs = await response.json();
    const total = rs.total_pages;

    if(page === 1)
        per_page = 1;
    else 
        per_page = page - 1;
    if(page === total)
        next_page = total;
    else
        next_page = page + 1;

    $('#main').empty();
    $('#pagination').empty();

    fillMovies(rs.results);

    $('#pagination').append(`
    <li class="page-item"><a class="page-link" href="#" 
    onclick="loadPageMovies('${reqStr}', ${per_page})">Previous</a></li>`);
    $('#pagination').append(`
    <li class="page-item"><a class="page-link" href="#"
    onclick="loadPageMovies('${reqStr}', ${next_page})">Next</a></li>`);
}

function fillMovies(ms)
{
    $('#main').empty();
    var imglink;

    for(const m of ms){
        imglink = `${img}${m.poster_path}`;
        $('#main').append(`           
            <div class="col-md-3 py-1">
                <div class="card shadow h-100" onclick="loadDetail(${m.id})">   
                    <img src="${imglink}" class="card-img-top"
                    alt="${m.title}">                
                    <div class="card-body">
                        <div class="card-title">
                        <h5 class="texthover">${m.title}</h5>
                        <div>Vote average: ${m.vote_average}</div>
                        <div>Release date: ${m.release_date}</div>
                        </div>
                    </div>
                </div>
                <div id="pagination-wrapper"></div>
            </div>
        `);
    };
}

function loadMoviesFromActor(ms){
    placeFill = '#main'
    $(placeFill).empty();

    for(const m of ms){ 
        loadMovie(m.id, m.name, placeFill);
    };
}

async function loadDetail(id)
{
    const reqStr = `https://api.themoviedb.org/3/movie/${id}?api_key=${api_key}`;

    loading();
    const response = await fetch(reqStr);
    const rs = await response.json();

    fillMovieDetail(rs);
}

function fillMovieDetail(rs){
    $('#main').empty();
    $('#pagination').empty();
    var imglink = `${img}${rs.poster_path}`;
    const reqStr = `https://api.themoviedb.org/3/movie/${rs.id}/reviews?api_key=${api_key}`;
    $('#main').append(`
    <div class="col">
    <div class="row">
    <div class="col-md-5">
      <img class="img-fluid" src="${imglink}" alt="${rs.title}">
    </div>
    <div class="col-md-7">
        <h1 class="my-1">${rs.title}</h1>
      <h3 class="my-3">Overview</h3>
      <p>${rs.overview}</p>
      <h3 class="my-3">Details</h3>
      <ul>
      <li>
      <span>Release year: </span>
      <span>${rs.release_date}</span>
      </li>
      <li>
      <span>Director: </span>
      <span id="directors"></span>
      </li>
      <li>
      <span>Genres:</span>
      <span id="genres"></span>
      </li>        
      </ul>
    </div>
    </div>
    <div>
        <H3><br/>Actors:</H3>
        <span id="actors"></span>
    </div>
    <div>
        <H3><br/>Reviews:</H3>
        <span id="reviews"></span>
        <nav>                      
        <ul class="pagination justify-content-center" id="paginationReviews"></ul>
        </nav>
    </div>
    </div>
    `);

    loadDirectors(rs.id);
    fillGenres(rs.genres);
    loadActorsFromMovie(rs.id);
    loadReviews(reqStr, 1, );
}

async function loadReviews(reqStr, page){
    const req = reqStr;
    var per_page, next_page;

    const newReqStr = `${req}&page=${page}`;
    const response = await fetch(newReqStr);
    const rs = await response.json();

    const totalpages = rs.total_pages

    if(page === 1)
        per_page = 1;
    else 
        per_page = page - 1;
    if(page === totalpages)
        next_page = totalpages;
    else
        next_page = page + 1;    

    $('#reviews').empty();
    $('#paginationReviews').empty();

    fillReviews(rs.results);

    $('#paginationReviews').append(`
    <li class="page-item"><a class="page-link" href="#" 
    onclick="loadReviews('${req}', ${per_page}, ${totalpages})">Previous</a></li>`);
    $('#paginationReviews').append(`
    <li class="page-item"><a class="page-link" href="#" 
    onclick="loadReviews('${req}', ${next_page}, ${totalpages})">Next</a></li>`);
}

function fillReviews(ms){
    for(const m of ms){
        $('#reviews').append(`
        <div class="card shadow card1">
        <h5 class="card-header">${m.author} <i>said:</i></h5>
        <div class="card-body">${m.content}</div>
        </div>
        `)
    }
}

async function loadDirectors(id){
    const reqStr = `https://api.themoviedb.org/3/movie/${id}/credits?api_key=${api_key}`;
    const response = await fetch(reqStr);
    const rs = await response.json();
    for(const m of rs.crew)
        if(m.job =='Director')
            {
                fillDirections(m);
            };
}

function fillDirections(m){
    $('#directors').append(`
    <span">${m.name}</span>
    `);
}

function fillGenres(rs){
    var i = 0;
    for(const m of rs){
        i=i+1;
        if(rs.length>i){
        $('#genres').append(`
        <span>${m.name},</span>
        `);
        } else {
        $('#genres').append(`
        <span>${m.name}</span>
        `);
        }
    }
}

async function loadActorsFromMovie(id){
    const reqStr = `https://api.themoviedb.org/3/movie/${id}/credits?api_key=${api_key}`;
    const response = await fetch(reqStr);
    const rs = await response.json();
    fillActorsFromMovie(rs.cast)    
}

function fillActorsFromMovie(rs){
    var i = 0;
    for(const m of rs){
        i=i+1;
        if(rs.length>i){
        $('#actors').append(`
        <span class="texthover" onclick="loadActorDetail(${m.id})">${m.name},</span>
        `);
        } else {
        $('#actors').append(`
        <span class="texthover" onclick="loadActorDetail(${m.id})">${m.name}</span>
        `);
        }
    }
}

async function loadActorDetail(id){
    const reqStr = `https://api.themoviedb.org/3/person/${id}?api_key=${api_key}`;
    const response = await fetch(reqStr);
    const rs = await response.json();
    fillActorDetail(rs);
}

function fillActorDetail(rs){
    $('#main').empty();
    var imglink = `${img}${rs.profile_path}`;
    var deathday;
    var gender;
    if(rs.deathday == null){
        deathday = ' ';
    }
    else{
        deathday = '- died: '+rs.deathday;
    }
    if(rs.gender == 0)
        gender = 'Not specified';
    else if(rs.gender == 1)
        gender = 'Female';
    else   
        gender = 'Male';
    $('#main').append(`
    <div class="col">
    <div class="row">
    <div class="col-md-5">
      <img class="img-fluid" src="${imglink}" alt="${rs.name}">
    </div>
    <div class="col-md-7">
      <h1 class="my-1">${rs.name}</h1>
      <ul>
      <li>
      <span>Born: ${rs.birthday}</span>
      <span>${deathday}<br/></span>
      </li>
      <li>
      <span>Place of birth: </span>
      <span>${rs.place_of_birth}</span>
      </li>
      <li>
      <span>Gender: </span>
      <span>${gender}</span>
      </li>
      </ul>
    </div>
    </div>
    <div>
        <H3><br/>Biography: </H3>
        <p>${rs.biography}</p>
    </div>
    <div>
        <H3><br/>Movies:</H3>
        <div class="row" id="movies"></div>
    </div>
    </div>
    `);

    loadMovie(rs.id, rs.name, '#movies');
}

async function loadMovie(id, name, placeFill){
    const reqStr =  `https://api.themoviedb.org/3/person/${id}/movie_credits?api_key=${api_key}`;
    const response = await fetch(reqStr);
    const rs = await response.json();
    fillMoviesFromActor(rs.cast, name, placeFill);
}

function fillMoviesFromActor(rs, name, placeFill){
    var imglink, character;
    for(const m of rs){
        imglink = `${img}${m.poster_path}`;
        if (m.character.length == 0)
            character = ' ';
        else
            character = '(' + name + ' is ' + m.character + ')';
        $(placeFill).append(`
        <div class="col-md-3 py-1">
                <div class="card shadow h-50" onclick="loadDetail(${m.id})">   
                    <img src="${imglink}" class="card-img-top"
                    alt="${m.title}">                
                    <div class="card-body">
                        <div class="card-title">
                        <h5 class="texthover">${m.title}</h5>
                        </div>
                        <p class="card-text">${character}</p>
                    </div>
                </div>
                <div id="pagination-wrapper"></div>
            </div>
            `);
    };
}

